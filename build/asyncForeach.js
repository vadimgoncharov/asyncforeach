(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
window.asyncForeach = window.asyncForeach || require('./asyncForeach');
},{"./asyncForeach":2}],2:[function(require,module,exports){
'use strict';

/**
 * Entry point
 * @module asyncForeach
 */

var util    = require('./util');
var iterate = require('./iterate');

/**
 * Asynchronously iterates over array
 * @param {Array} array
 * @param {module:asyncForeach~iteratorStepCallback} iteratorStepCb
 * @param {module:asyncForeach~allIterationsDoneCallback} [allDone]
 */
function asyncForeach(array, iteratorStepCb, allDone) {
	if (!util.isArray(array)) {
		throw new Error('First argument must be an array, but it\'s ' + util.toString(array));
	}

	if (!util.isFunction(iteratorStepCb)) {
		throw new Error('Second argument must be a function, but it\'s ' + util.toString(iteratorStepCb));
	}

	if (!util.isFunction(allDone) && !util.isUndefined(allDone)) {
		throw new Error('Third argument must be a function or undefined, but it\'s ' + util.toString(allDone));
	}

	iterate(array, 0, iteratorStepCb, allDone);
}

/**
 * This callback is fired on every iteration
 * @callback module:asyncForeach~iteratorStepCallback
 * @param {*} item
 * @param {number} index
 * @param {module:asyncForeach~iterationDoneCallback} done
 */

/**
 * User calls this callback to finish current iteration and go to the next iteration
 * @callback module:asyncForeach~iterationDoneCallback
 */

/**
 * This callback is fired when all iterations completed
 * @callback module:asyncForeach~allIterationsDoneCallback
 */

module.exports = asyncForeach;

},{"./iterate":3,"./util":4}],3:[function(require,module,exports){
'use strict';

/**
 * Main logic
 * @module asyncForeach/iterate
 */

/**
 *
 * @param {Array} array
 * @param {number} index
 * @param {module:asyncForeach~iteratorStepCallback} iteratorStepCb
 * @param {module:asyncForeach~allIterationsDoneCallback} [allDone]
 */
function iterate(array, index, iteratorStepCb, allDone) {
	if (index >= array.length) {
		return allDone ? allDone() : void 0;
	}

	iteratorStepCb(array[index], index, function() {
		iterate(array, index + 1, iteratorStepCb, allDone);
	});
}

module.exports = iterate;

},{}],4:[function(require,module,exports){
'use strict';

/**
 * Util collection
 * @module asyncForeach/util
 */

/**
 * Returns "[object TypeName]" string
 * @param {*} val
 * @returns {String}
 */
function toString(val) {
	return ({}).toString.call(val);
}

/**
 * Returns true if value is array
 * @param {*} val
 * @returns {boolean}
 */
function isArray(val) {
	return Array.isArray(val);
}

/**
 * Returns true if value is function
 * @param {*} val
 * @returns {boolean}
 */
function isFunction(val) {
	return typeof val === 'function';
}

/**
 * Returns true if value is undefined
 * @param {*} val
 * @returns {boolean}
 */
function isUndefined(val) {
	return typeof val === 'undefined';
}

module.exports = {
	toString: toString,
	isArray: isArray,
	isFunction: isFunction,
	isUndefined: isUndefined
};

},{}]},{},[1]);
