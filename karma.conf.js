'use strict';

module.exports = function(karma) {
	karma.set({
		frameworks: ['qunit', 'browserify'],
		files: [
			'test/runner.js'
		],
		reporters: ['coverage', 'spec'],
		browsers: ['PhantomJS'],
		preprocessors: {
			'test/runner.js': ['coverage', 'browserify']
		},
		basePath: __dirname,

		coverageReporter: {
			type: 'text',
			dir : 'coverage/'
		},

		browserify: {
			transform: ['browserify-istanbul'],
			debug: false
		},

		coverage: {

		},
		singleRun: true,
		autoWatch: false
	});
};
