var asyncForeach    = require('../../lib/asyncForeach');
var util            = require('../../lib/util');

var firstArgErrorText   = 'First argument must be an array, but it\'s ';
var secondArgErrorText  = 'Second argument must be a function, but it\'s ';
var thirdArgErrorText   = 'Third argument must be a function or undefined, but it\'s ';

QUnit.test('asyncForeach throws exception on invalid first argument', function(assert) {
	assert.throws(function() {
		asyncForeach({}, function() {});
	}, new Error(firstArgErrorText + util.toString({})));

	assert.throws(function() {
		asyncForeach(void 0, function() {});
	}, new Error(firstArgErrorText + util.toString(void 0)));
});

QUnit.test('asyncForeach throws exception on invalid second argument', function(assert) {
	assert.throws(function() {
		asyncForeach([], {});
	}, new Error(secondArgErrorText + util.toString({})));

	assert.throws(function() {
		asyncForeach([], void 0);
	}, new Error(secondArgErrorText + util.toString(void 0)));
});

QUnit.test('asyncForeach throws exception on invalid third argument', function(assert) {
	assert.throws(function() {
		asyncForeach([], function() {}, null);
	}, new Error(thirdArgErrorText + util.toString(null)));
});

QUnit.test('asyncForeach starts iteration if all arguments is valid', function(assert) {
	asyncForeach([], function(){});
	assert.ok(true, 'asyncForeach complete');
});
