var asyncForeach = require('../../lib/asyncForeach');

QUnit.test('asyncForeach iterates over all values in array with correct values in callback', function(assert) {
	var done = assert.async();

	var array = [5,6,7];
	var arrayIndex = -1;

	asyncForeach(array, function(value, index, next) {
		arrayIndex += 1;
		assert.equal(value, array[arrayIndex]);
		next();
	}, function() {
		assert.ok(true, 'all iterations complete');
		assert.equal(arrayIndex, 2);
		done();
	});
});

QUnit.test('asyncForeach correctly iterates asynchronously', function(assert) {
	var done = assert.async();

	var array = [5,6];
	var lastValue = 0;

	asyncForeach(array, function(value, index, next) {
		lastValue = value;
		setTimeout(function() {
			next();
		}, 50);
	});

	setTimeout(function() {
		assert.equal(lastValue, 6);
		done();
	}, 200);
});

QUnit.test('asyncForeach never ends if next arg not called', function(assert) {
	var done = assert.async();

	var array = [5];
	var outerValue = null;
	var reachDoneCb = false;

	asyncForeach(array, function(item, index, next){
		// next()
	}, function() {
		reachDoneCb = true;
	});

	setTimeout(function() {
		assert.equal(reachDoneCb, false);
		done();
	}, 100);
});