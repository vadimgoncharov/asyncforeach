var util = require('../../lib/util');

QUnit.test('util.toString returns correct value', function(assert) {
	assert.equal(util.toString(''), '[object String]');
	assert.equal(util.toString({}), '[object Object]');
	assert.equal(util.toString([]), '[object Array]');
});

QUnit.test('util.isArray returns correct value', function(assert) {
	assert.equal(util.isArray([]), true);
	assert.equal(util.isArray({}), false);
});

QUnit.test('util.isUndefined returns correct value', function(assert) {
	assert.equal(util.isUndefined(void 0), true);
	assert.equal(util.isUndefined(null), false);
});

QUnit.test('util.isFunction returns correct value', function(assert) {
	assert.equal(util.isFunction(function() {}), true);
	assert.equal(util.isFunction([]), false);
	assert.equal(util.isFunction(''), false);
});