'use strict';

/**
 * Main logic
 * @module asyncForeach/iterate
 */

/**
 *
 * @param {Array} array
 * @param {number} index
 * @param {module:asyncForeach~iteratorStepCallback} iteratorStepCb
 * @param {module:asyncForeach~allIterationsDoneCallback} [allDone]
 */
function iterate(array, index, iteratorStepCb, allDone) {
	if (index >= array.length) {
		return allDone ? allDone() : void 0;
	}

	iteratorStepCb(array[index], index, function() {
		iterate(array, index + 1, iteratorStepCb, allDone);
	});
}

module.exports = iterate;
