'use strict';

/**
 * Entry point
 * @module asyncForeach
 */

var util    = require('./util');
var iterate = require('./iterate');

/**
 * Asynchronously iterates over array
 * @param {Array} array
 * @param {module:asyncForeach~iteratorStepCallback} iteratorStepCb
 * @param {module:asyncForeach~allIterationsDoneCallback} [allDone]
 */
function asyncForeach(array, iteratorStepCb, allDone) {
	if (!util.isArray(array)) {
		throw new Error('First argument must be an array, but it\'s ' + util.toString(array));
	}

	if (!util.isFunction(iteratorStepCb)) {
		throw new Error('Second argument must be a function, but it\'s ' + util.toString(iteratorStepCb));
	}

	if (!util.isFunction(allDone) && !util.isUndefined(allDone)) {
		throw new Error('Third argument must be a function or undefined, but it\'s ' + util.toString(allDone));
	}

	iterate(array, 0, iteratorStepCb, allDone);
}

/**
 * This callback is fired on every iteration
 * @callback module:asyncForeach~iteratorStepCallback
 * @param {*} item
 * @param {number} index
 * @param {module:asyncForeach~iterationDoneCallback} done
 */

/**
 * User calls this callback to finish current iteration and go to the next iteration
 * @callback module:asyncForeach~iterationDoneCallback
 */

/**
 * This callback is fired when all iterations completed
 * @callback module:asyncForeach~allIterationsDoneCallback
 */

module.exports = asyncForeach;
