'use strict';

/**
 * Util collection
 * @module asyncForeach/util
 */

/**
 * Returns "[object TypeName]" string
 * @param {*} val
 * @returns {String}
 */
function toString(val) {
	return ({}).toString.call(val);
}

/**
 * Returns true if value is array
 * @param {*} val
 * @returns {boolean}
 */
function isArray(val) {
	return Array.isArray(val);
}

/**
 * Returns true if value is function
 * @param {*} val
 * @returns {boolean}
 */
function isFunction(val) {
	return typeof val === 'function';
}

/**
 * Returns true if value is undefined
 * @param {*} val
 * @returns {boolean}
 */
function isUndefined(val) {
	return typeof val === 'undefined';
}

module.exports = {
	toString: toString,
	isArray: isArray,
	isFunction: isFunction,
	isUndefined: isUndefined
};
